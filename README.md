# Superfluous
#### Live demo: https://tpz.gitlab.io/Superfluous
Configurable startpage that lets you easily add and remove links within the page without editing the code.
You can add up to five groups with up to 12 links in each group for a total of 60 links.
![Demo video](img/demo.webm)
### Usage
Hover over the image to the left to display the configuration options and click on any non-button area to toggle the image on or off.

Start by adding groups by clicking on the '+' below 'ADD OR REMOVE GROUPS'.  
To add links to a group, click on the number below 'CONFIGURE GROUPS' to the group you want to add links to. Then add cells to that group by clicking on the '+' below 'ADD OR REMOVE CELLS'.

Hover over a group to expand it and show its links.
Now when you click on an unassigned cell, the browser will prompt you to enter a URL and label for that link.  

Use `Ctrl + Left Click` on a cell remove the link which is assigned to it.

Additional configuration can be found by clicking on 'Extra Config'.
### Theme
There are four themes that you can switch between and if you want to add your own you can:

- Edit one of the existing themes in the [theme](css/themes.css) file.

Or

- Add a new one by uncommenting the `.theme5` block in the [theme](css/themes.css) file and give the variables inside the values you desire. Then uncomment the `<button>` tag with the id `theme5` in the [index](index.html) file.

You can add more than five by just incrementing the number after the `theme` in both files.
### Local Storage
The page depends on Local Storage to store the links and configuration, and therefore won't work in "private" or "incognito" browsing.
