(function(){
"use strict";
var editGroup;
var settings = {
	visibleGroups: 0,
	width: 800,
	height: 400
};
var groups = [];


// Runs the init function when the DOM has loaded
document.addEventListener('DOMContentLoaded', init, false);
function init(){
	var i;
	for (i = 1; i <= 5; i++) {
		groups.push({
			element: document.getElementById('group' + i),
			cells: 0
		});
	}
	if (localStorage.visibleGroups > 0) {
		loadGroups();
	}
	if (localStorage.wrapperWidth !== undefined) {
		resize(true);
	}
	if (localStorage.theme !== undefined) {
		document.body.className = localStorage.theme;
	}
	loadCells();
	loadLinks();
	coverListener();

	document.getElementById("addGBtn").addEventListener("click", addGroup);
	document.getElementById("removeGBtn").addEventListener("click", addGroup);
	document.getElementById("addCBtn").addEventListener("click", addCell);
	document.getElementById("removeCBtn").addEventListener("click", addCell);

	document.getElementById('clearAll').addEventListener('click', clearAllStorage);
	document.getElementById('clearGroup').addEventListener('click', clearGroup);
	document.getElementById('export').addEventListener("click", exportLocalStorage);
	document.getElementById('import').addEventListener("click", importLocalStorage);

	document.getElementById('backG').addEventListener('click', changeView);
	document.getElementById('backE').addEventListener('click', changeView);
	document.getElementById('extraBtn').addEventListener('click', changeView);

	document.getElementById('incWidth').addEventListener('click', resize);
	document.getElementById('decWidth').addEventListener('click', resize);
	document.getElementById('incHeight').addEventListener('click', resize);
	document.getElementById('decHeight').addEventListener('click', resize);

	i = 1;
	while (document.getElementById('theme' + i)) {
		document.getElementById('theme' + i).addEventListener('click', themeSwitcher);
		i++;
	}

}



// Toggle the image
function toggleCover() {
	document.getElementById('conf').classList.toggle('toggle');
}
function coverListener() {
	var cTitle = document.getElementsByClassName("confTitle");
	for (var i = 0; i < cTitle.length; i++) {
		cTitle[i].addEventListener("click", toggleCover);
	}
}

// Changing conf view
function changeView() {
	if (document.getElementById('confMain').classList.toggle('activeView')) {
		document.getElementById('confGroup').classList.remove('activeView');
		document.getElementById('confExtra').classList.remove('activeView');
		groups[editGroup].element.classList.remove('hovered');
	}
	else if (this.classList.contains("groupConfBtn")) {
		editGroup = this.innerHTML - 1;
		document.getElementById('confGroup').classList.add('activeView');
		groups[editGroup].element.classList.add('hovered');
	}
	else {
		document.getElementById('confExtra').classList.add('activeView');
	}
}


function addGroup() {
	var confBtn;
	// When clicking +
	if (this === undefined || this.id === "addGBtn" && settings.visibleGroups < 5) {
		confBtn = document.getElementsByClassName("groupConfBtn")[settings.visibleGroups];
		confBtn.style.display = 'block';
		confBtn.addEventListener("click", changeView);
		groups[settings.visibleGroups].element.style.display = 'flex';

		settings.visibleGroups++;
	}
	// When clicking -
	else if (this.id === 'removeGBtn' && settings.visibleGroups > 0) {
		settings.visibleGroups--;

		groups[settings.visibleGroups].element.style.display = '';
		confBtn = document.getElementsByClassName("groupConfBtn")[settings.visibleGroups];
		confBtn.style.display = '';
	}
	document.documentElement.style.setProperty('--groups', settings.visibleGroups);
	// Saves to local storage
	localStorage.visibleGroups = settings.visibleGroups;
}


function addCell() {
	var xRow;
	var xCell;
	var thisGroup = groups[editGroup];

	// When clicking +
	if (this === undefined || this.id === "addCBtn" && thisGroup.cells <= 11) {
		if (thisGroup.cells % 3 === 0) {
			xRow = thisGroup.element.getElementsByClassName('row')[thisGroup.cells / 3];
			xRow.style.display = 'flex';
		}
		xCell = thisGroup.element.getElementsByClassName('cell')[thisGroup.cells];
		xCell.style.display = 'flex';

		xCell.group = editGroup;
		xCell.cellIndex = thisGroup.cells;
		xCell.addEventListener('click', cellClick);

		thisGroup.cells++;
	}
	// When clicking -
	else if (this.id === 'removeCBtn' && thisGroup.cells >= 1) {
		thisGroup.cells--;
		if (thisGroup.cells % 3 === 0) {
			xRow = thisGroup.element.getElementsByClassName('row')[thisGroup.cells / 3];
			xRow.style.display = '';
		}
		xCell = thisGroup.element.getElementsByClassName('cell')[thisGroup.cells];
		xCell.style.display = '';
	}

	localStorage["group" + editGroup] = thisGroup.cells;
}

// Clicking an empty cell
function cellClick(event) {
	// If the clicked link does not have a value, ask the user to assign one.
	if (this.getAttribute("href") === '' || this.getAttribute("href") === 'null') {
		var newUrl = prompt('Enter your desired URL.\nExample: "https://dressupgames.com/"', "https://");
        var newText = prompt("Enter the label for this link");
		if (newText !== null && newUrl !== null && newUrl.length >= 10) {
			this.innerHTML = newText;
			this.href = newUrl;
			storeLink(this, newText, newUrl);
		}
		event.preventDefault();
	}
	// If ctrl-key is held down, ask user if the link should be reset.
	else if (event.ctrlKey) {
		if (confirm("Reset link?")) {
			this.href = "";
			this.innerHTML = "Click to assign link!";
			storeLink(this, null, null);
		}
		event.preventDefault();
	}
}

function storeLink(element, newText, newUrl) {
	var texts = [];
	var urls = [];
	if (localStorage["group" + element.group + "texts"]) {
		texts = JSON.parse(localStorage["group" + element.group + "texts"]);
		urls = JSON.parse(localStorage["group" + element.group + "urls"]);
	}
	texts[element.cellIndex] = newText;
	urls[element.cellIndex] = newUrl;
	localStorage["group" + element.group + "texts"] = JSON.stringify(texts);
	localStorage["group" + element.group + "urls"] = JSON.stringify(urls);
}

// Loads and adds groups from storage
function loadGroups() {
	var vGroups = localStorage.visibleGroups;
	for (var i = 0; i < vGroups; i++) {
		addGroup();
	}
}

// Loads and adds cells from storage
function loadCells() {
	for (var i = 0; i < 5; i++) {
		editGroup = i;
		var groupX = localStorage["group" + i];
		for (var j = 0; j < groupX; j++) {
			addCell();
		}
	}
}

// Loads and applies links from storage
function loadLinks() {
	var tempGroup;
	var cellLink;
	var texts;
	var urls;

	for (var g = 0; g < 5; g++) {
		tempGroup = document.getElementsByClassName('group')[g];
		if (!localStorage["group" + g + "texts"]) {
			continue;
		}
		texts = JSON.parse(localStorage["group" + g + "texts"]);
		urls = JSON.parse(localStorage["group" + g + "urls"]);

		for (var c = 0; c < texts.length; c++) {
			if (texts[c] !== null || undefined) {
				cellLink = tempGroup.getElementsByClassName('cell')[c];
				cellLink.innerHTML = texts[c];
				cellLink.href = urls[c];
			}
		}
	}
}

// resize wrapper
function resize(pageLoad) {
	if (pageLoad === true) {
		settings.height = parseInt(localStorage.wrapperHeight);
		settings.width = parseInt(localStorage.wrapperWidth);
		document.documentElement.style.setProperty('--height', settings.height + "px");
		document.documentElement.style.setProperty('--width', settings.width + "px");
	}
	else {
		switch(this.id) {
			case "incWidth":
				settings.width += 100;
				document.documentElement.style.setProperty('--width', settings.width + "px");
				break;

			case "decWidth":
				settings.width -= 100;
				document.documentElement.style.setProperty('--width', settings.width + "px");
				break;

			case "incHeight":
				settings.height += 50;
				document.documentElement.style.setProperty('--height', settings.height + "px");
				break;

			case "decHeight":
				settings.height -= 50;
				document.documentElement.style.setProperty('--height', settings.height + "px");
				break;

			default:
				alert("Error");
				break;
		}
		localStorage.wrapperWidth = settings.width;
		localStorage.wrapperHeight = settings.height;
	}
}

// Switching theme
function themeSwitcher() {
	var theme = "theme" + this.innerHTML;
	document.body.className = theme;
	localStorage.theme = theme;
}


// Export, import and clear config functions
function exportLocalStorage() {
    var backup = {};
    var keys = [];
    var value;
    for (var i = 0; i < 5; i++) {
        keys.push("group" + i, "group" + i + "texts", "group" + i + "urls");
    }
    keys.push("visibleGroups", "theme", "wrapperWidth", "wrapperHeight");

    for (i = 0; i < keys.length; i++) {
        value = localStorage[keys[i]];
        if (value !== undefined) {
            backup[keys[i]] = value;
        }
    }

    var href = 'data:text/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(backup));
    var link = document.createElement("a");
    link.href = href;
    link.download = "startpageConf.json";
    document.body.appendChild(link);
    link.click();
    link.remove();
}

function importLocalStorage() {
    var input = document.createElement('input');
    input.type = "file";
    input.accept = ".json";

    input.click();

    input.addEventListener("change", function () {
        var file = input.files[0];

        if (file) {
            var reader = new FileReader();
            reader.onload = function () {
                var backup = JSON.parse(reader.result);
                for (var key in backup) {
                    localStorage.setItem(key, backup[key]);
                }
                alert('Imported ' + Object.keys(backup).length + ' items.');
                location.reload();
            };
            reader.readAsText(file);
        }
        else {
            alert("Failed to load file");
        }
    });
}

function clearGroup() {
    if (confirm("Are you sure you want to reset this group?\nThis cannot be undone.")) {
        localStorage.removeItem("group" + editGroup + "texts");
        localStorage.removeItem("group" + editGroup + "urls");
        location.reload();
    }
}

function clearAllStorage() {
    if (confirm("Are you sure you want to reset all config?\nThis cannot be undone.")) {
        for (var i = 0; i < 5; i++) {
            localStorage.removeItem("group" + i);
            localStorage.removeItem("group" + i + "texts");
            localStorage.removeItem("group" + i + "urls");
        }
        localStorage.removeItem("visibleGroups");
        localStorage.removeItem("theme");
        localStorage.removeItem("wrapperWidth");
        localStorage.removeItem("wrapperHeight");
        location.reload();
    }
}

})();